#!/bin/bash

# Errors should propagate
set -e

NEW_ISSUE_ID=$(jq '.iid' issue.json)
NEW_ISSUE_URL=$(jq '.web_url' issue.json)
MILESTONE_ID=$(cat milestone_id-www.txt)
ISSUE_ID=$(basename $ISSUE_URL)
TITLE=$(curl "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/issues/$ISSUE_ID" | jq '.title')
# bash slugify from: https://gist.github.com/oneohthree/f528c7ae1e701ad990e6
SLUG=$(echo $TITLE | iconv -t ascii//TRANSLIT | sed -E 's/[^a-zA-Z0-9]+/-/g' | sed -E 's/^-+|-+$//g' | tr A-Z a-z)

jq \
    --arg TITLE "WIP: Release post for: $TITLE" \
    --arg BRANCH_NAME "$GITLAB_USERNAME-$SLUG" \
    --arg MILESTONE_ID $MILESTONE_ID \
    --arg GITLAB_USER_ID $GITLAB_USER_ID \
    --rawfile TEMPLATE ./mr_template.md \
    '.source_branch = $BRANCH_NAME | .title = $TITLE | .assignee_ids[0] = $GITLAB_USER_ID | .milestone_id = $MILESTONE_ID | .description = $TEMPLATE' \
    new_mr.json > mr_payload.json

curl \
  --data "@mr_payload.json" \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN"  \
  "https://gitlab.com/api/v4/projects/$WWW_PROJECT_ID/merge_requests" > mr_output.json

MR_IID=$(jq '.iid' mr_output.json)

jq \
    --arg BODY "Closes $NEW_ISSUE_URL" \
    '.body = $BODY' \
    mr_note.json > note_payload.json

curl \
  -v \
  --data "@note_payload.json" \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN"  \
  "https://gitlab.com/api/v4/projects/$WWW_PROJECT_ID/merge_requests/$MR_IID/notes"

jq '.web_url' mr_output.json
