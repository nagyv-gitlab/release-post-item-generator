#!/bin/bash
set -e

NEW_ISSUE_ID=$(jq '.iid' issue.json)
ISSUE_ID=$(basename $ISSUE_URL)
TITLE=$(curl "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/issues/$ISSUE_ID" | jq '.title')
# bash slugify from: https://gist.github.com/oneohthree/f528c7ae1e701ad990e6
SLUG=$(echo $TITLE | iconv -t ascii//TRANSLIT | sed -E 's/[^a-zA-Z0-9]+/-/g' | sed -E 's/^-+|-+$//g' | tr A-Z a-z)

jq \
  --arg BRANCH_NAME "$GITLAB_USERNAME-$SLUG" \
  --arg FILENAME "data/release_posts/unreleased/$GITLAB_USERNAME-$SLUG.yml" \
  --arg MESSAGE "Branch created for release post #$NEW_ISSUE_ID" \
  --rawfile TEMPLATE ./template.yaml \
  '.branch = $BRANCH_NAME | .actions[0].content = $TEMPLATE | .commit_message = $MESSAGE | .actions[0].file_path = $FILENAME' \
  new_branch.json > branch_payload.json

curl \
  --data "@branch_payload.json" \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN"  \
  "https://gitlab.com/api/v4/projects/$WWW_PROJECT_ID/repository/commits"
