#!/bin/bash
set -e

MILESTONE_ID=$(cat milestone_id.txt)
ISSUE_ID=$(basename $ISSUE_URL)
TITLE=$(curl "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/issues/$ISSUE_ID" | jq '.title')

jq \
  --arg TITLE "Release post for: $TITLE" \
  --arg GITLAB_USER_ID $GITLAB_USER_ID \
  --arg MILESTONE_ID $MILESTONE_ID \
  --rawfile TEMPLATE ./issue_template.md \
  '.title = $TITLE | .assignee_ids[0] = $GITLAB_USER_ID | .milestone_id = $MILESTONE_ID | .description = $TEMPLATE' \
  new_issue.json > issue_payload.json

curl \
  --data "@issue_payload.json" \
  --header "Content-Type: application/json" \
  --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
  "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/issues" > issue.json

ERR=$(jq '.message.error' issue.json)
echo $ERR

