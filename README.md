# Release post item generator

This is a collection of ci jobs to be run by GitLab PM's to generate a boilerplate release post item.

## What is does

```
Given
- an `issue id`
- and a `gitlab handle`
- a string `primary / secondary`
When 
- you start a new CI job
Then
- it creates an issue assigned to the `gitlab handle`
- it creates a new branch in `www-gitlab-com` with the name `new issue id-gitlab handle-issue id`
- adds `primary / secondary` yaml to the new branch
- starts a new MR for the branch assigned to `gitlab handle`
```

## Extra possibilities

- grab some text from the issue and use it for the inital description in the yaml
- add the generated issue to a Release Post Epic
