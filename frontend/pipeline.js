"use strict"

const PROJECT_ID = `${process.env.CI_PROJECT_ID}`

const data = new URLSearchParams(window.location.search.slice(1))
const content = document.getElementById('content')
const template = document.getElementById('template')

const statusToAlertClass = {
  pending: 'info',
  running: 'primary',
  failed: 'danger',
  canceled: 'warning',
  success: 'success'
}

window.hasActivePipeline = false

async function getPipelineData(pipeline_id) {
  try {
    const response = await fetch(`https://gitlab.com/api/v4/projects/${PROJECT_ID}/pipelines/${pipeline_id}`)
    return response.json()
  } catch(err) {
    console.error(err)
    return {
      id: pipeline_id,
      status: 'error'
    }
  }
}

function manageReload() {
  if(window.hasActivePipeline) return
  
  window.hasActivePipeline = setTimeout(() => window.location.reload(), 5000)
}

function createPipelineBox(pipeline) {
  // It would be better to first clone the node, then edit its children.
  // But querySelector needs access to the whole dom. So it does not work.
  template.querySelector('.pipeline_id').innerHTML = pipeline.id
  template.querySelector('.pipeline_url').href = pipeline.web_url
  template.querySelector('.alert').classList.add(`alert-${statusToAlertClass[pipeline.status] || 'warning'}`)
  template.querySelector('.status').innerHTML = pipeline.status
  template.querySelector('.pipeline_remove').dataset.pipeline = pipeline.id
  const newNode = template.cloneNode(true)
  newNode.classList.remove('d-none')
  newNode.querySelector('.pipeline_remove').addEventListener('click', removePipeline)
  content.appendChild(newNode)
}

function getStoredPipelines() {
  const stored = window.localStorage.getItem('pipelines')
  if(!stored) {
    return []
  }
  return JSON.parse(stored)
}

function setStoredPipelines(pipelines) {
  window.localStorage.setItem('pipelines', JSON.stringify(pipelines))
}

function removePipeline(ev) {
  ev.preventDefault()
  console.log('pipeline', ev.target.dataset.pipeline)
  let stored = getStoredPipelines()
  const index = stored.indexOf(ev.target.dataset.pipeline);
  if (index > -1) {
    stored.splice(index, 1);
  }
  setStoredPipelines(stored)
  ev.target.closest('.card').remove()
}

if(data.get('pipeline_ids')) {
  const stored = getStoredPipelines()
  // store only unique values
  setStoredPipelines([...new Set(stored.concat(data.get('pipeline_ids').split(',')))])
}
const pipelines = getStoredPipelines()

if(pipelines.length > 0) {
  document.getElementById('no-pipelines').classList.add('d-none')
}

pipelines.forEach(async (pipeline_id) => {
  const pipeline = await getPipelineData(pipeline_id)

  createPipelineBox(pipeline)
  if(pipeline.status != 'passed' && !window.hasActivePipeline) {
    manageReload()
  }
})
