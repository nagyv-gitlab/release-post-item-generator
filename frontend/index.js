"use strict"

const PIPELINE_STATUS_PAGE = document.getElementById('pipeline_url').href
const PIPELINE_URL = 'https://gitlab.com/api/v4/projects/16988169/trigger/pipeline'
const PIPELINE_TRIGGER = 'b9dffc9b0f7030e26129965bf1585d'

function knownValues() {
  document.getElementById('gitlab_handle').value = localStorage.getItem('gitlab_handle')
  document.getElementById('milestone').value = localStorage.getItem('milestone')
}

function saveValues(data) {
  localStorage.setItem('gitlab_handle', data.get('GITLAB_USERNAME'))
  localStorage.setItem('milestone', data.get('MILESTONE'))
}

async function startPipeline(data) {
  let response
  let jsonData = {
    token: PIPELINE_TRIGGER,
    ref: 'master',
    variables: {}
  }
  data.forEach((value, key) => jsonData.variables[key] = value);
  try {
    const fetchResponse = await fetch(PIPELINE_URL, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(jsonData)
    })
    response = await fetchResponse.json()
  } catch(err) {
    console.error(err)
    return
  }
  
  if(response.error) {
    console.log('Request failed', {response, jsonData})
  } else {
    console.log(response)
    window.location.href = `${PIPELINE_STATUS_PAGE}?pipeline_ids=${response.id}`
  }
}

knownValues()

const generatorForm = document.getElementById('generator')
generatorForm.addEventListener('submit', (event) => {
  event.preventDefault()
  const data = new FormData(generatorForm)
  saveValues(data)
  startPipeline(data).catch(err => console.error(err))
})
